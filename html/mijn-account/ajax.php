<?php
    /**
    * AJAX
    * 
    * Handles all the front-end AJAX requests
    */

    include('../custom-includes/init.class.php');
    $init = new init();

    // Checking for minimum PHP version
    if (version_compare(PHP_VERSION, '5.3.7', '<')) {
        exit("Sorry, Simple PHP Login does not run on a PHP version smaller than 5.3.7 !");
    } else if (version_compare(PHP_VERSION, '5.5.0', '<')) {
        require_once("custom-libraries/password_compatibility_library.php");
    }

    // create a login object. when this object is created, it will do all login/logout stuff automatically
    $account = new account($init->database);

    // Only process requests when a user is logged in
    if ($account->isUserLoggedIn() == 'ok') {
        if (isset($_POST["action"])) {
            switch ($_POST["action"]){
                case 'log-device-id': 
                    $userId = $_SESSION['user_id'];
                    $deviceId = $_POST["param1"];
                    $ip = $_SERVER['REMOTE_ADDR'];
                    if ($result = $account->logUserAction($userId, $deviceId, $ip)) {
                        echo 'ok';
                    }
                    else {
                        echo 'not ok';
                    }
                    break;

                case 'get-view-url':
                    if ($account->isUserAuthorized() == 'ok'){
                        $productId = $_POST["param1"];

                        $viewer = new viewer($init->database);
                        $viewUrl = $viewer->getViewUrl($productId,'v1');
                        if (isset($viewUrl)){
                            echo $viewUrl;
                        }
                        else {
                            echo 'not ok';
                        }
                    }
                    else {
                        echo 'not ok';
                    }
                    break;

                case 'get-view-url-2':
                    if ($account->isUserAuthorized() == 'ok'){
                        $productId = $_POST["param1"];

                        $viewer = new viewer($init->database);
                        $viewUrl = $viewer->getViewUrl($productId,'v2');
                        if (isset($viewUrl)){
                            echo $viewUrl;
                        }
                        else {
                            echo 'not ok';
                        }
                    }
                    else {
                        echo 'not ok';
                    }
                    break;

                case 'remove-view-url':
                    $viewUrl = $_POST["param1"];

                    $viewer = new viewer($init->database);
                    if ($result = $viewer->deleteViewUrl($viewUrl)){
                        echo 'ok';
                    }
                    else {
                        echo 'not ok';
                    }
                    break;
            }
        }
    }
    else {
        echo 'Log aub eerst in.';
    }
?>
