<?php
    include('http://exovd.nl/custom-includes/init.class.php');
    $init = new init();
    setlocale(LC_ALL, 'nl_NL');


    // create a login object. when this object is created, it will do all login/logout stuff automatically
    //$account = new account($init->database);

    // Determine the page target
    $target = 'dashboard';
    if (isset($_GET['page'])) {
        $target = $_GET['page'];
    }

    switch ($target) {
        case "dashboard":
            $textTitle = 'Dashboard | ExamenOverzicht';
            $textMetaDesc = 'Mijn dashboard op ExamenOverzicht.';
            $textHeader = 'Dashboard';
            $imgHeader = 'exovd.nl/custom-images/header-dashboard.png';

            break;    
        case "profiel":
            $textTitle = 'Mijn profiel | ExamenOverzicht';
            $textMetaDesc = 'Mijn profiel op ExamenOverzicht.';
            $textHeader = 'Mijn profiel';
            $imgHeader = 'exovd.nl/custom-images/header-mijn-profiel.png';

            break;
        case "samenvattingen":
            $textTitle = 'Mijn samenvattingen | ExamenOverzicht';
            $textMetaDesc = 'Mijn samenvattingen op ExamenOverzicht.';
            $textHeader = 'Mijn samenvattingen';
            $imgHeader = 'exovd.nl/custom-images/header-mijn-samenvattingen.png';

            break;
        case "bestellingen":
            $textTitle = 'Mijn bestellingen | ExamenOverzicht';
            $textMetaDesc = 'Mijn bestellingen op ExamenOverzicht.';
            $textHeader = 'Mijn bestellingen';
            $imgHeader = 'exovd.nl/custom-images/header-mijn-bestellingen.png';

            break;
    }
?>

<!DOCTYPE html lang="nl">
<head>
    <?php echo '<base href="//exovd.nl/" />'; ?>
    <meta charset="UTF-8">
    <title><?php echo $textTitle; ?></title>
    <meta name="description" content="<?php echo $textMetaDesc; ?>"/>
    <meta name="author" content="ExamenOverzicht"/> 
    <meta name="robots" content="noindex, nofollow"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="canonical" href="https://www.examenoverzicht.nl/mijn-account" />
    <link rel="icon" type="image/png" href="http://exovd.nl/favicon.ico"/>
    <script src="http://exovd.nl/custom-js/jquery-1.11.1.min.js"></script>
</head>

<body>
    <div id="overlay"></div>
    <div id="page-container">
        <div id="book-top"></div>
        <div id="book-middle-container">
            <div id="book-left">
                <div id="book-left-inner">
                    <?php echo Setting::baseUrlHttps.'testest' ?>
                </div>
            </div>
            <div id="content-container">
                <div id="logo-course"><a href="http://exovd.nl/mijn-account"><img src="http://exovd.nl/custom-images/my-examenoverzicht.png" alt="Mijn ExamenOverzicht" width="504" height="52"></a></div>
                <div id="logo-course-parent"><a href="http://exovd.nl"><img src="http://exovd.nl/custom-images/back-to-examenoverzicht.png" alt="Terug naar ExamenOverzicht" width="306" height="38"></a></div>
                <?php // Display the header text ?>
                <div id="account-page-left">
                    <?php
                        if ($account->isUserLoggedIn() == 'ok') {
                        ?>
                        <div class="course-menu-container-switch">
                            <div class="course-menu-top">
                                <img src="http://exovd.nl/custom-images/header-mijn-account.png" class="header-image" alt="Mijn account">
                            </div>
                            <div class="course-menu-content">
                                <div class="course-menu-item"><a href="http://exovd.nl/mijn-account"><i class="fa fa-home"></i> Dashboard</a></div>
                                <div class="course-menu-item"><a href="http://exovd.nl/mijn-account/profiel"><i class="fa fa-child"></i> Mijn profiel</a></div>
                                <div class="course-menu-item"><a href="http://exovd.nl/mijn-account/bestellingen"><i class="fa fa-shopping-cart"></i> Mijn bestellingen</a></div>
                                <div class="course-menu-item"><a href="http://exovd.nl/mijn-account/samenvattingen"><i class="fa fa-book"></i> Mijn samenvattingen</a></div>
                                <div class="course-menu-item"><a href="http://exovd.nl/mijn-account?logout"><i class="fa fa-sign-out"></i> Uitloggen</a></div>
                            </div>
                            <div class="course-menu-footer"></div>
                        </div>
                        <?php
                        }  
                    ?>
                </div>                
                <?php
                    if ($account->isUserLoggedIn() == 'ok') {

                        // Insert the same menu, but this time in mobile style
                    ?>
                    <nav itemtype="http://schema.org/SiteNavigationElement" itemscope="itemscope" role="navigation" class="mobile-menu">
                        <span class="mobile-menu-header">Navigeer naar:</span>
                        <ul class="menu">
                            <li class="menu-item"><a href="http://exovd.nl/mijn-account"><i class="fa fa-home"></i> Dashboard</a></li>
                            <li class="menu-item"><a href="http://exovd.nl/mijn-account/profiel"><i class="fa fa-child"></i> Mijn profiel</a></li>
                            <li class="menu-item"><a href="http://exovd.nl/mijn-account/bestellingen"><i class="fa fa-shopping-cart"></i> Mijn bestellingen</a></li>
                            <li class="menu-item"><a href="http://exovd.nl/mijn-account/samenvattingen"><i class="fa fa-book"></i> Mijn samenvattingen</a></li>
                            <li class="menu-item"><a href="http://exovd.nl/mijn-account?logout"><i class="fa fa-sign-out"></i> Uitloggen</a></li>
                        </ul>
                        <div class="responsive-nav-close"></div>
                    </nav>
                    <?php

                        echo '<div id="page-content-container" class="mobile-clear-left">';

                        echo '<div id="page-content-top"><div class="responsive-nav-icon"></div><h1 class="assistive-element">'.$textHeader.'</h1>';
                        echo '<img src="'.$imgHeader.'" class="header-image" alt="'.$textHeader.'"></div>';
                        echo '<div class="paper-writing">';

                        // Load the page specific content
                        switch ($target) {
                            case "dashboard":

                                /****************************************************
                                *                                                   *
                                * Dashboard
                                *                                                   *
                                ****************************************************/

                                echo '<p>Welkom op je ExamenOverzicht account.</p>';
                                echo '<p>Op deze omgeving kun je het overzicht van <a href="http://exovd.nl/mijn-account/bestellingen">je bestellingen</a> zien, nieuwe bestellingen aan je account <a href="'.Setting::baseUrlHttps.'/mijn-account/bestellingen">koppelen</a> en natuurlijk je aangeschafte <a href="'.Setting::baseUrlHttps.'/mijn-account/samenvattingen">digitale samenvattingen bekijken</a>.</p><br>';
                                echo '<p>Nog geen digitale samenvattingen in bezit? Bestel deze dan nu op <a href="http://exovd.nl" target="_blank">ExamenOverzicht.nl</a>.</p><br>';
                                break;

                            case "profiel":

                                /****************************************************
                                *                                                   *
                                * Profiel
                                *                                                   *
                                ****************************************************/

                                echo '<p>Ingelogd als: '.$_SESSION['user_name'].'</p>';
                                echo '<p>Gebruikers ID: '.$_SESSION['user_id'].'</p>';
                                echo '<p>Email adres: '.$_SESSION['user_email'].'</p>';
                                echo '<p>Login methode: '.$_SESSION['user_login_method'].'</p>';

                                break;
                        }

                        echo '</div>';
                        echo '<div id="page-content-bottom"></div>'; 
                        echo '</div>';
                    } else {


                        /****************************************************
                        *                                                   *
                        * Not logged in
                        *                                                   *
                        ****************************************************/

                        echo '<div class="flex-container"><div id="login-container" class="content-area"><h1 class="h1-sm">Log in op ExamenOverzicht</h1><br>';

                        echo '<a class="btn btn-block btn-social btn-lg btn-facebook" href="mijn-account?provider=Facebook"><span class="fa fa-facebook"></span> Log in met Facebook</a>';
                        echo '<a class="btn btn-block btn-social btn-lg btn-instagram" href="mijn-account?provider=Instagram"><span class="fa fa-instagram"></span> Log in met Instagram</a>';
                        echo '<a class="btn btn-block btn-social btn-lg btn-google" href="mijn-account?provider=Google"><span class="fa fa-google"></span> Log in met Google</a>';

                        echo '<br><div class="breakable-line"><span class="in-breakable-line">of</span></div><br>';
                    ?>

                    <!-- login form -->
                    <form id="user-login-form" method="post" action="dashboard">
                        <div class="form-group">
                            <div class="form-label-3">Email</div>
                            <input type="email" class="form-control" name="email" required />
                        </div>
                        <div class="form-group">
                            <div class="form-label-3">Wachtwoord</div>
                            <input type="password" class="form-control" name="password" pattern=".{6,}" required autocomplete="off" />
                        </div>
                        <input type="hidden" name="form-name" value="user-login" />
                        <div class="wide-and-center"><input type="submit" class="btn btn-default" value="Inloggen" /></div>
                    </form>
                    <p>Wachtwoord vergeten? Neem contact met ons op via info@examenoverzicht.nl.</p>

                    <?php
                        echo '</div>';

                        echo '<div id="registration-container" class="content-area"><h1 class="h1-sm">Geen account?<br>Maak er een aan</h1><br>';

                        echo '<a class="btn btn-block btn-social btn-lg btn-facebook" href="mijn-account?provider=Facebook"><span class="fa fa-facebook"></span> Registreer met Facebook</a>';
                        echo '<a class="btn btn-block btn-social btn-lg btn-instagram" href="mijn-account?provider=Instagram"><span class="fa fa-instagram"></span> Registreer met Instagram</a>';
                        echo '<a class="btn btn-block btn-social btn-lg btn-google" href="mijn-account?provider=Google"><span class="fa fa-google"></span> Registreer met Google</a>';

                        echo '<br><div class="breakable-line"><span class="in-breakable-line">of</span></div><br>';
                    ?>

                    <!-- register form -->
                    <form id="user-reg-form" method="post" action="mijn-account">
                        <div class="form-group">
                            <div class="form-label-3">Email</div>
                            <input type="email" class="form-control" name="email" pattern="(?!(^[.-].*|[^@]*[.-]@|.*\.{2,}.*)|^.{254}.)([a-zA-Z0-9!#$%&'*+\/=?^_`{|}~.-]+@)(?!-.*|.*-\.)([a-zA-Z0-9-]{1,63}\.)+[a-zA-Z]{2,15}" required />
                        </div>
                        <div class="form-group">
                            <div class="form-label-3">Naam</div>
                            <input type="text" class="form-control" name="name" required />
                        </div>

                        <div class="form-group">
                            <div class="form-label-3">Wachtwoord (min. 6 tekens lang)</div>
                            <input type="password" class="form-control" name="password" pattern=".{6,}" required autocomplete="off" />
                        </div>
                        <div class="form-group">
                            <div class="form-label-3">Herhaal wachtwoord</div>
                            <input type="password" class="form-control" name="password_repeat" pattern=".{6,}" required autocomplete="off" />
                        </div>
                        <input type="hidden" name="form-name" value="user-reg" />
                        <div class="wide-and-center"><input type="submit" class="btn btn-default" value="Registreren" /></div>
                    </form>

                    <?php
                        echo '</div></div>';
                    }
                ?>

            </div>
            <div id="book-right"></div>
        </div>           
        <div id="book-bottom"></div>
    </div>

    <link rel="stylesheet" href="http://exovd.nl/custom-css/bootstrap.css?v=666" type="text/css" property="stylesheet"/>
    <link rel="stylesheet" href="http://exovd.nl/custom-css/bootstrap-social.css" type="text/css" property="stylesheet">
    <link rel="stylesheet" href="http://exovd.nl/custom-css/style.css?v=666" type="text/css" property="stylesheet"/>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" property="stylesheet">

    </body>
</html>