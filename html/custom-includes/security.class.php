<?php
    /**
    * Security class
    * 
    * All operations regarding the security in the system
    */

    class security
    {
        var $salt = 'u2r7ExovSalt8b&^d91';

        var $databaseClass;

        public function __construct($databaseClass)
        {
            if($databaseClass != "")
                $this->databaseClass = $databaseClass;
        }

        public function data_validation_check($var, $function)
        {
            switch($function)
            {
                case "clean_hash": {$data = $this->clean_hash($var);break;} 
                case "clean_email": {$data = $this->clean_email($var);break;} 
                case "clean_ipv4": {$data = $this->clean_ipv4($var);break;} 
                case "clean_integer": {$data = $this->clean_integer($var);break;} 
                case "clean_date": {$data = $this->clean_date($var);break;}      
                case "clean_string": {$data = $this->clean_string($var);break;} 
                case "clean_housenumber": {$data = $this->clean_housenumber($var);break;}
                case "clean_postalcode": {$data = $this->clean_postalcode($var);break;}
                case "skip": {$data = $var;break;} 
            }
            return $data;
        }

        //Clean the hash variable
        function clean_hash($hash)
        {
            if (filter_var($hash, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z0-9]+$/")))) 
                return $hash;
            else
                return false;
        }

        //Clean the house number variable
        function clean_housenumber($var)
        {
            if (filter_var($var, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^\d+[a-zA-Z]*$/"))))
                return $var;
            else
                return false;
        }
        
        //Clean the postal code variable
        function clean_postalcode($var)
        {
            if (filter_var($var, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i"))))
                return $var;
            else
                return false;
        }

        //Clean the email variable
        function clean_email($email)
        {
            if(filter_var(trim($email), FILTER_VALIDATE_EMAIL))
                return trim($email);
            else 
                return false;
        }

        //Clean the string variable
        function clean_string($string)
        {
            $string = filter_var($string, FILTER_SANITIZE_STRING);
            $string = $this->databaseClass->filter($string);
            return $string;
        }

        //Clean the ipv4 variable
        function clean_ipv4($ipv4)
        {
            if(filter_var($ipv4, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE))
                return $ipv4; 
            else
                return false;
        }

        //Clean the integer variable 
        function clean_integer($integer)
        {
            // Int(0) gets cast by PHP to false, therefore the '===0' is added
            // Note: +0 and -0 are still not allowed
            if(filter_var($integer, FILTER_VALIDATE_INT)===0 || !filter_var($integer, FILTER_VALIDATE_INT)===False)
                return $integer;
            else 
                return false;
        }

        //Clean the date variable 
        function clean_date($date)
        {
            //Check the format (DD-MM-YYYY)
            if (preg_match ("/^([0-9]{2})-([0-9]{2})-([0-9]{4})$/", $date, $dateParts))
            {
                //Check the date
                if (checkdate($dateParts[2],$dateParts[1],$dateParts[3]))
                    return $date;
                else    
                    return false;
            }   
            else
                return false; 
        }
    }
?>
